using System.Collections.Generic;
using System.Linq;
using Domain;

namespace Persistence
{
    public class Seed
    {
        public static void SeedData(DataContext context)
        {
            if (!context.Grinds.Any())
            {
                var grinds = new List<Grind>
                {
                    new Grind
                    {
                        Title = "French Press - Medium",
                        CoffeeMaker = "frenchpress",
                        Coarseness = 5,
                        Temperature = 200,
                        Water = 15,
                        Coffee = 1,
                        Description = "Ristretto blue mountain cultivar qui so spoon as aromatic ristretto at robust that, coffee, dripper ristretto so, chicory fair trade ristretto cortado coffee. Dark chicory frappuccino that single origin arabica qui caffeine white java et rich in, iced, aroma macchiato, ut, rich beans caffeine robust and carajillo caffeine. Galão espresso, turkish organic aroma trifecta aged medium white, blue mountain con panna turkish, body french press spoon siphon bar café au lait. Iced, and body black ut spoon shop aromatic, id aroma ut brewed caffeine doppio medium irish. Viennese, spoon, dripper seasonal milk filter, single origin, java extraction, trifecta aroma crema aftertaste, decaffeinated, in, body lungo chicory black chicory sweet brewed. Saucer fair trade cortado a acerbic, medium, bar coffee caffeine, filter sweet, seasonal, turkish fair trade, filter, half and half, roast cream steamed, roast foam decaffeinated siphon iced. Grounds, froth viennese aged, est, acerbic dark robust a lungo as chicory robust variety. Est, macchiato brewed, to go caffeine extraction organic, rich, strong french press, grounds caffeine milk and filter skinny roast skinny to go.",
                    },
                    new Grind
                    {
                        Title = "Aeropress",
                        CoffeeMaker = "aeropress",
                        Coarseness = 2,
                        Temperature = 175,
                        Water = 12,
                        Coffee = 1,
                        Description = "Espresso blue mountain cultivar qui so spoon as aromatic ristretto at robust that, coffee, dripper ristretto so, chicory fair trade ristretto cortado coffee. Dark chicory frappuccino that single origin arabica qui caffeine white java et rich in, iced, aroma macchiato, ut, rich beans caffeine robust and carajillo caffeine. Galão espresso, turkish organic aroma trifecta aged medium white, blue mountain con panna turkish, body french press spoon siphon bar café au lait. Iced, and body black ut spoon shop aromatic, id aroma ut brewed caffeine doppio medium irish. Viennese, spoon, dripper seasonal milk filter, single origin, java extraction, trifecta aroma crema aftertaste, decaffeinated, in, body lungo chicory black chicory sweet brewed. Saucer fair trade cortado a acerbic, medium, bar coffee caffeine, filter sweet, seasonal, turkish fair trade, filter, half and half, roast cream steamed, roast foam decaffeinated siphon iced. Grounds, froth viennese aged, est, acerbic dark robust a lungo as chicory robust variety. Est, macchiato brewed, to go caffeine extraction organic, rich, strong french press, grounds caffeine milk and filter skinny roast skinny to go.",
                    },
                    new Grind
                    {
                        Title = "Moka pot",
                        CoffeeMaker = "perculator",
                        Coarseness = 2,
                        Temperature = 212,
                        Water = 15,
                        Coffee = 1,
                        Description = "Espresso blue mountain cultivar qui so spoon as aromatic ristretto at robust that, coffee, dripper ristretto so, chicory fair trade ristretto cortado coffee. Dark chicory frappuccino that single origin arabica qui caffeine white java et rich in, iced, aroma macchiato, ut, rich beans caffeine robust and carajillo caffeine. Galão espresso, turkish organic aroma trifecta aged medium white, blue mountain con panna turkish, body french press spoon siphon bar café au lait. Iced, and body black ut spoon shop aromatic, id aroma ut brewed caffeine doppio medium irish. Viennese, spoon, dripper seasonal milk filter, single origin, java extraction, trifecta aroma crema aftertaste, decaffeinated, in, body lungo chicory black chicory sweet brewed. Saucer fair trade cortado a acerbic, medium, bar coffee caffeine, filter sweet, seasonal, turkish fair trade, filter, half and half, roast cream steamed, roast foam decaffeinated siphon iced. Grounds, froth viennese aged, est, acerbic dark robust a lungo as chicory robust variety. Est, macchiato brewed, to go caffeine extraction organic, rich, strong french press, grounds caffeine milk and filter skinny roast skinny to go.",
                    },
                    new Grind
                    {
                        Title = "Pour over",
                        CoffeeMaker = "dripper",
                        Coarseness = 3,
                        Temperature = 205,
                        Water = 15,
                        Coffee = 1,
                        Description = "Ristretto blue mountain cultivar qui so spoon as aromatic ristretto at robust that, coffee, dripper ristretto so, chicory fair trade ristretto cortado coffee. Dark chicory frappuccino that single origin arabica qui caffeine white java et rich in, iced, aroma macchiato, ut, rich beans caffeine robust and carajillo caffeine. Galão espresso, turkish organic aroma trifecta aged medium white, blue mountain con panna turkish, body french press spoon siphon bar café au lait. Iced, and body black ut spoon shop aromatic, id aroma ut brewed caffeine doppio medium irish. Viennese, spoon, dripper seasonal milk filter, single origin, java extraction, trifecta aroma crema aftertaste, decaffeinated, in, body lungo chicory black chicory sweet brewed. Saucer fair trade cortado a acerbic, medium, bar coffee caffeine, filter sweet, seasonal, turkish fair trade, filter, half and half, roast cream steamed, roast foam decaffeinated siphon iced. Grounds, froth viennese aged, est, acerbic dark robust a lungo as chicory robust variety. Est, macchiato brewed, to go caffeine extraction organic, rich, strong french press, grounds caffeine milk and filter skinny roast skinny to go.",
                    },
                    new Grind
                    {
                        Title = "Cold-spresso",
                        CoffeeMaker = "coldbrewer",
                        Coarseness = 5,
                        Temperature = 80,
                        Water = 10,
                        Coffee = 1,
                        Description = "Ristretto blue mountain cultivar qui so spoon as aromatic ristretto at robust that, coffee, dripper ristretto so, chicory fair trade ristretto cortado coffee. Dark chicory frappuccino that single origin arabica qui caffeine white java et rich in, iced, aroma macchiato, ut, rich beans caffeine robust and carajillo caffeine. Galão espresso, turkish organic aroma trifecta aged medium white, blue mountain con panna turkish, body french press spoon siphon bar café au lait. Iced, and body black ut spoon shop aromatic, id aroma ut brewed caffeine doppio medium irish. Viennese, spoon, dripper seasonal milk filter, single origin, java extraction, trifecta aroma crema aftertaste, decaffeinated, in, body lungo chicory black chicory sweet brewed. Saucer fair trade cortado a acerbic, medium, bar coffee caffeine, filter sweet, seasonal, turkish fair trade, filter, half and half, roast cream steamed, roast foam decaffeinated siphon iced. Grounds, froth viennese aged, est, acerbic dark robust a lungo as chicory robust variety. Est, macchiato brewed, to go caffeine extraction organic, rich, strong french press, grounds caffeine milk and filter skinny roast skinny to go.",
                    },
                };

                context.Grinds.AddRange(grinds);
                context.SaveChanges();
            }
        }
    }
}