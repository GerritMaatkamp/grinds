import React, { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import '../../styles/App.scss';
import NavBar from '../../components/nav/NavBar';
import { Dashboard } from '../../components/grinds/dashboard/Dashboard';
import DefaultModal from '../../components/modals/default';
import DeleteModal from '../../components/modals/delete';
import GrindStore from '../stores/grindStore';
import { Route, withRouter, RouteComponentProps } from 'react-router-dom';
import GrindDetail from '../../components/grinds/details/GrindDetail';
import { Footer } from '../../components/nav/Footer';

const App: React.FC<RouteComponentProps> = () => {
  const grindStore = useContext(GrindStore);
  const { deleteModalOpen, formModalOpen } = grindStore;

  return (
    <div className="App">

      <NavBar />

      <div className={`App-body ${formModalOpen ? 'blur' : ''}`}>
        <Route exact path="/" component={Dashboard} />
        <Route path="/grinds/:id" component={GrindDetail} />
      </div>

      {formModalOpen && <DefaultModal />}
      {deleteModalOpen && <DeleteModal /> }
      <Footer/>      
    </div>
  );
};

export default withRouter(observer(App));
