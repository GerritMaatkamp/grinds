import { observable, action, computed } from 'mobx';
import { createContext } from 'react';
import { IGrindV1 } from '../models/grind';
import agent from '../api/agent';

class GrindStore {
  @observable grindsRegistry = new Map();
  @observable grind: IGrindV1 | null = null;
  @observable selectedGrind: IGrindV1 | undefined;
  @observable loadingInitial = false;
  @observable submitting = false;
  @observable formModalOpen: boolean = false;
  @observable deleteModalOpen: boolean = false;
  @observable cups = 1;

  @computed get grindsArray() {
    return Array.from(this.grindsRegistry.values());
  }

  @action loadGrinds = async () => {
    this.loadingInitial = true;
    try {
      const grinds = await agent.Grinds.list();
      grinds.forEach((grind) => {
        this.grindsRegistry.set(grind.id, grind);
      });
      this.loadingInitial = false;
    } catch (error) {
      console.log(error);
    }
  };

  @action loadGrind = async (id: string) => {
    let grind = this.getGrind(id);
    if (grind) {
      this.grind = grind;
    } else {
      try {
        this.grind = await agent.Grinds.details(id);
      } catch (error) {
        console.log(error);
      }
    }
  };

  getGrind = (id: string) => {
    return this.grindsRegistry.get(id);
  }

  @action createGrind = async (grind: IGrindV1) => {
    this.submitting = true;
    try {
      await agent.Grinds.create(grind);
      this.grindsRegistry.set(grind.id, grind);
      this.submitting = false;
    } catch (error) {
      console.log(error);
      this.submitting = false;
    }
  };

  @action editGrind = async (grind: IGrindV1) => {
    this.submitting = true;
    try {
      await agent.Grinds.update(grind.id, grind);
      this.grindsRegistry.set(grind.id, grind);
      this.submitting = false;
    } catch (error) {
      this.submitting = false;
      console.log(error);
    }
  };

  @action deleteGrind = async (grind: IGrindV1 | undefined) => {
    this.loadingInitial = true;
    try {
      await agent.Grinds.delete(grind!.id).then(() => {
        console.log('delete success');
        this.closeDeleteDialog();
      });
      this.grindsRegistry.delete(grind!.id);

    } catch (error) {
      console.log(error);
      
    }
  }

  @action disableScroll() {
    // Get the current page scroll position
    let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    let scrollLeft = window.pageXOffset || document.documentElement.scrollLeft;
    // if any scroll is attempted, set this to the previous value
    window.onscroll = function () {
      window.scrollTo(scrollLeft, scrollTop);
    };
  }

  @action showFormToggle = () => {
    window.scrollTo(window.pageYOffset, window.pageXOffset);
    this.formModalOpen = !this.formModalOpen;
    console.log(this.formModalOpen);
  };

  @action showEditForm = (id: string | null) => {
    window.scrollTo(window.pageYOffset, window.pageXOffset);
    this.selectedGrind = this.grindsRegistry.get(id);
    this.formModalOpen = !this.formModalOpen;
  };

  @action closeForm = () => {
    this.formModalOpen = !this.formModalOpen;
    window.onscroll = function () {};
  };

  @action showDeleteDialog = (id: string) => {
    window.scrollTo(window.pageYOffset, window.pageXOffset);
    this.selectedGrind = this.grindsRegistry.get(id);
    this.deleteModalOpen = !this.deleteModalOpen;
  };

  @action closeDeleteDialog = () => {
    this.deleteModalOpen = !this.deleteModalOpen;
    window.onscroll = function () { };
  };

  @action capitalizeFirst = (string: string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  @action addCup = () => {
    this.cups++;
    console.log('this.cups');
    console.log(this.cups);
  }
  
  @action subtractCup = () => {
    this.cups--;
    console.log('this.cups');
    console.log(this.cups);
  }
}

export default createContext(new GrindStore());
