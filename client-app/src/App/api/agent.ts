import axios, { AxiosResponse } from 'axios';
import { IGrindV1 } from '../models/grind';

axios.defaults.baseURL = 'http://localhost:5000/api';

// axios.interceptors.response.use(undefined, (error) => {
//     console.log(error.response);
// });

const responseBody = (response: AxiosResponse) => response.data;

const request = {
  get: (url: string) => axios.get(url).then(responseBody),
  post: (url: string, body: {}) => axios.post(url, body).then(responseBody),
  put: (url: string, body: {}) => axios.put(url, body).then(responseBody),
  del: (url: string) => axios.delete(url).then(responseBody),
};

const Grinds = {
  list: (): Promise<IGrindV1[]> => request.get('/grinds'),
  details: (id: string) => request.get(`/grinds/${id}`),
  create: (grind: IGrindV1) => request.post('/grinds', grind),
  update: (id: string, grind: IGrindV1) => request.put(`/grinds/${id}`, grind),
  delete: (id: string) => request.del(`/grinds/${id}`),
};

export default {
  Grinds,
};
