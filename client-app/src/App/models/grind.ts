export interface IGrindV1 {
  id: string;
  title: string;
  coffeeMaker: string;
  coarseness: number;
  temperature: number;
  water: number;
  coffee: number;
  description: string;
}
