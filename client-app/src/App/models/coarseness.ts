export interface ICoarsenessV1 {
  value: number;
  name: string;
  example: string;
}

export const coarsenessTypes: ICoarsenessV1[] = [
  { value: 1, name: 'Extra Fine', example: 'Powdered Sugar' },
  { value: 2, name: 'Fine', example: 'Table Salt' },
  { value: 3, name: 'Medium', example: 'Sea Salt' },
  { value: 4, name: 'Mid Coarse', example: 'Well you know, Coarse' },
  { value: 5, name: 'Coarse', example: 'Barely even ground' },
];
