export interface ICoffeeMakerV1 {
  name: string;
  display: string;
}

export const coffeeMakers: ICoffeeMakerV1[] = [
  { name: 'perculator', display: 'Moka Pot / Perculator' },
  { name: 'frenchpress', display: 'French Press' },
  { name: 'aeropress', display: 'Aeropress' },
  { name: 'coldbrewer', display: 'Cold Brewer' },
  { name: 'dripper', display: 'Pour Over' },
];
