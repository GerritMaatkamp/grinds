import React, { useContext } from 'react';
import GrindStore from '../../App/stores/grindStore';

import { observer } from 'mobx-react-lite';

import logo from '../../logo.svg';
import add from '../../add.svg';
import '../../styles/navbar.scss';
import { Link } from 'react-router-dom';

const NavBar = () => {
  const grind = useContext(GrindStore);

  return (
    <div>
      <div className="App-navbar">
        <Link to="/">
          <img src={logo} className="App-navbar-logo" alt="Grinds" />
        </Link>
        <div className="buttons">
          <button className="App-navbar-add" onClick={() => grind.showEditForm(null)} >
            <img src={add} className="App-navbar-add-image" alt="Grinds" />
          </button>
        </div>
      </div>
    </div>
  );
};

export default observer(NavBar);
