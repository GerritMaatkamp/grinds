import React from 'react';
import '../../styles/footer.scss';
import icon_email from '../../icon_email.svg';
import icon_phone from '../../icon_phone.svg';
import icon_gitlab from '../../icon_gitlab.svg';

export const Footer = () => {
    return (
        <div className="footer">
            <ul className="footer-contact">
                <li className="footer-contact-item">
                    <img src={icon_phone} alt="908 463 2727"/>
                    <a href="callto: (908)463 27 27">(908)463 27 27</a> 
                </li>
                <li className="footer-contact-item">
                    <img src={icon_email} alt="908 463 2727"/>
                    <a href="mailto: gerrit.maatkamp@gmail.com">gerrit.maatkamp@gmail.com</a>
                </li>
                <li className="footer-contact-item">
                    <img src={icon_gitlab} alt="https://gitlab.com/GerritMaatkamp/grinds"/>
                    <a href="https://gitlab.com/GerritMaatkamp/grinds" target="_blank">Grinds Gitlab repo</a> 
                </li>
            </ul>
        </div>
    )
}
