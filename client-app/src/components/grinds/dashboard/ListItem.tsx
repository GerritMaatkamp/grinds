import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { IGrindV1 } from '../../../App/models/grind';
import GrindStore from '../../../App/stores/grindStore';
import '../../../styles/listitem.scss';

import edit from '../../../edit.svg';
import deleteIcon from '../../../deleteIcon.svg'
import view from '../../../view.svg'

interface IProps {
  grind: IGrindV1;
}

export const ListItem: React.FC<IProps> = ({ grind }) => {
  const grindStore = useContext(GrindStore);
  const { capitalizeFirst, showEditForm, showDeleteDialog } = grindStore;

  return (
    <article className="grinds-list-item">
      <div className="grinds-list-item-card">
        <div className="grinds-list-item-card-header">
          <h3>{capitalizeFirst(grind.title)}</h3>
          <p>{capitalizeFirst(grind.coffeeMaker)}</p>
        </div>
        <div className="grinds-list-item-card-body">
          <Link 
            className="grinds-list-item-card-body-image"
            to={`/grinds/${grind.id}`}>
            <img
              src={`assets/img/${grind.coffeeMaker}.png`}
              alt={grind.coffeeMaker}
              />
          </Link>
        </div>
      </div>

      <div className="grinds-list-item-edit">
        <div className="grinds-list-item-edit-item" onClick={() => showEditForm(grind.id)}>
          <img src={edit} alt="" className="grinds-list-item-edit-item-image"/>
        </div>
        <Link 
          to={`/grinds/${grind.id}`}
          className="grinds-list-item-edit-item" >
          <img src={view} alt="" className="grinds-list-item-edit-item-image grinds-list-item-edit-item-image__view"/>
        </Link>
        <div className="grinds-list-item-edit-item" onClick={() => showDeleteDialog(grind.id)}>
          <img src={deleteIcon} alt="" className="grinds-list-item-edit-item-image"/>
        </div>
      </div>
    </article>
  );
};
