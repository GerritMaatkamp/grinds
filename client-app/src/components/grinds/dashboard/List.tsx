import React, { useEffect, useContext } from 'react';
import { observer } from 'mobx-react-lite';
import { ListItem } from './ListItem';
import GrindStore from '../../../App/stores/grindStore';

const List = () => {
  const grindStore = useContext(GrindStore);
  const { grindsRegistry: grinds, loadGrinds, loadingInitial, grindsArray } = grindStore;

  useEffect(() => {
    loadGrinds();
  }, [loadGrinds]);

  return (
    <div>
      <ul>
        <ul className="App-body-list">
          {grindsArray.map((grind) => (
            <li key={grind.id}>
              <ListItem grind={grind} />
            </li>
          ))}
        </ul>
      </ul>
    </div>
  );
};

export default observer(List);
