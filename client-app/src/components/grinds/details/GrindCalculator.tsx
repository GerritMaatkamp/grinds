import React, { useContext } from 'react';
import GrindStore from '../../../App/stores/grindStore';

import icon_cup from '../../../img/cup.svg'
import coffee_bean from '../../../img/coffee_bean.svg'
import { observer } from 'mobx-react-lite';

interface IProps {
    grams: number;
}

const GrindCalculator: React.FC<IProps> = ({grams}) => {
    const grindStore = useContext(GrindStore);
    const { cups, addCup, subtractCup } = grindStore;
    
    return (
        <div className="cup-calculator">
            <img className="cup-calculator-cup" src={icon_cup} alt="cups water" />
            <button className="button" onClick={subtractCup}>-</button>
            <p> {cups} </p>
            <button className="button" onClick={() => addCup()}>+</button>
            <img src={coffee_bean} alt="TBSP coffee" />
            <p>
                {grams * cups} gr
            </p>
            <p>
                {(grams * cups) / 5} tbsp
            </p>
        </div>
    )
}

export default observer(GrindCalculator);