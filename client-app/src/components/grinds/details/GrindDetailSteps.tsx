import React from 'react'
import { observer } from 'mobx-react-lite'

interface IProps {
    steps: string[] | undefined;
}

const GrindDetailSteps: React.FC<IProps> = ({steps}) => {
    
    return (
        <div className="detail-header-text-steps">
            <h2>Steps how to make this coffee</h2>
            <ul>
                {steps?.map(step => (
                    <li key={steps?.indexOf(step)}>
                        <h3>Step  {Number(steps?.indexOf(step)) + 1} </h3>
                        <p>{step}</p>
                    </li>
                ))}
            </ul>
        </div>
    )
}

export default observer(GrindDetailSteps);