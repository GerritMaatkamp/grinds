import React from 'react';

import grinder from '../../../img/grinder.svg';
import frenchpress from '../../../img/frenchpress.svg';
import aeropress from '../../../img/aeropress.svg';
import dripper from '../../../img/dripper.svg';
import coldbrewer from '../../../img/coldbrewer.svg';
import perculator from '../../../img/perculator.svg';

interface IProps {
    coffeeMaker: string;
    temperature: number;
    coarseness: number;
}

let renderCoarseness = (n: number) => {
    switch (n) {
        case 1:
            return <p>Extra fine</p>
            break;
        case 2:
            return <p>Fine</p>
            break
        case 3:
            return <p>Medium</p>
            break
        case 4:
            return <p>Mid coarse</p>
            break
        case 5:
            return <p>Coarse</p>
            break
        default:
            break;
    }
}

export const GrindInfoGraphic: React.FC<IProps> = ({ coffeeMaker, temperature, coarseness }) => {
    return (
        <div className="detail-coffeemaker">
            <div className="detail-coffeemaker-image">
                {(coffeeMaker === "frenchpress") && <img src={frenchpress} alt="frenchpress" />}
                {(coffeeMaker === "aeropress") && <img src={aeropress} alt="aeropress" />}
                {(coffeeMaker === "dripper") && <img src={dripper} alt="dripper" />}
                {(coffeeMaker === "coldbrewer") && <img src={coldbrewer} alt="coldbrewer" />}
                {(coffeeMaker === "perculator") && <img src={perculator} alt="perculator" />}
            </div>
            <div className="detail-coffeemaker-coarseness ">
            <div className={`detail-coffeemaker-coarseness-type ${(coarseness < 3) ? 'fine' : 'medium'}`}>
                    <img src={grinder} alt="grinder" />
                    {renderCoarseness(Number(coarseness))}
                </div>
            </div>
            <div className="detail-temperature">
                <h3>
                    {temperature}°
                </h3>
            </div>
        </div>
    )
}
