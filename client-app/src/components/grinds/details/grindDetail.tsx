import React, { useContext, useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import GrindStore from '../../../App/stores/grindStore';
import { observer } from 'mobx-react-lite';
import '../../../styles/grindDetail.scss';

import GrindDetailSteps from './GrindDetailSteps';
import GrindCalculator from './GrindCalculator';
import { GrindInfoGraphic } from './GrindInfoGraphic';

interface DetailParams {
    id: string;
}

const GrindDetail: React.FC<RouteComponentProps<DetailParams>> = ({match}) => {
    const grindStore = useContext(GrindStore);
    const { loadGrind, grind, capitalizeFirst } = grindStore;

    useEffect(() => {
        loadGrind(match.params.id);
    }, [loadGrind])

    return (
        <div className="detail">
            <div className="detail-header">
                <div className="detail-header-text">
                    <h1 className="detail-header-text-title">
                        { grind && capitalizeFirst(grind.title) }
                    </h1>
                    <div className="detail-header-text-subtitle">
                        <h4>
                            { grind && capitalizeFirst(grind?.coffeeMaker) }
                        </h4>
                        {grind && <GrindCalculator grams={grind?.coffee} />}
                    </div>
                    <GrindDetailSteps steps={grind?.description.split('/')} />
                </div>

                {grind && 
                <GrindInfoGraphic
                    coffeeMaker={grind?.coffeeMaker.toLowerCase()} 
                    temperature={grind?.temperature} 
                    coarseness={grind?.coarseness} 
                />}
            </div>
        </div>
    )
}

export default observer(GrindDetail);
