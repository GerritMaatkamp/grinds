import React, { useContext, FormEvent, useState } from 'react';
import { IGrindV1 } from '../../App/models/grind';
import { ICoarsenessV1, coarsenessTypes } from '../../App/models/coarseness';
import { ICoffeeMakerV1, coffeeMakers } from '../../App/models/coffeeMaker';
import GrindStore from '../../App/stores/grindStore';
import { observer } from 'mobx-react-lite';
import { Form } from 'semantic-ui-react';
import { v4 as uuid } from 'uuid';
import '../../styles/modal.scss';

const DefaultModal = () => {
  const grindStore = useContext(GrindStore);
  let { createGrind, editGrind, selectedGrind, disableScroll } = grindStore;

  disableScroll();


  const initForm = () => {
    if (selectedGrind) {
      return selectedGrind;
    } else {
      return {
        id: '',
        title: '',
        coffeeMaker: '',
        description: '',
        coarseness: 0,
        temperature: 200,
        water: 1,
        coffee: 16,
      }
    }
  }

  const [grind, setGrind] = useState<IGrindV1>(initForm);

  const handleInputChange = (
    event: FormEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = event.currentTarget;
    if (name === 'coarseness') {
      console.log(value);

    }
    setGrind({ ...grind, [name]: value });
  };

  const submitForm = () => {
    let newGrind: IGrindV1 = {
      ...grind,
      title: grind.title.toLowerCase(),
      coffeeMaker: grind.coffeeMaker.toLowerCase(),
      water: 1,
      coffee: Number(grind.coffee),
      coarseness: Number(grind.coarseness),
      temperature: Number(grind.temperature),
    }

    if (grind.id.length === 0) {
      let newGrind: IGrindV1 = { ...grind, id: uuid(), coarseness: Number(grind.coarseness) }
      createGrind(newGrind);
    } else {
      editGrind(newGrind);
    }
    grindStore.closeForm();
  };

  return (
    <div className="modal">
      <div id="overlay" onClick={grindStore.closeForm}></div>
      <div className="modal-content">
        <div className="modal-content-header">
          {(!grind.id) && <h2>New Grind</h2>}
          {grind.id && <h2>Edit {grind?.title}</h2>}
        </div>
        <div className="modal-content-body">
          <Form>
            <Form.Input
              name="title"
              label="Title"
              placeholder="title"
              onChange={handleInputChange}
              value={grind?.title}
            />
            <Form.Field
              label="Coffee Maker"
              name="coffeeMaker"
              control="select"
              value={grind?.coffeeMaker}
              onChange={handleInputChange}
            >
              <option value="None">Pick a Coffee Maker</option>
              {coffeeMakers.map((coffeeMaker: ICoffeeMakerV1) => (
                <option value={coffeeMaker.name}>{coffeeMaker.display}</option>
              ))}
            </Form.Field>
            <Form.Field
              label="Coarseness"
              name="coarseness"
              control="select"
              type="number"
              onChange={handleInputChange}
              value={grind?.coarseness}>
              <option value={0}>Select coarseness</option>
              {coarsenessTypes.map((coarsenessType: ICoarsenessV1) => (
                <option value={coarsenessType.value}>
                  {coarsenessType.name}
                </option>
              ))}
            </Form.Field>
            <Form.Input
              label="Temperature"
              name="temperature"
              type="number"
              placeholder="212"
              onChange={handleInputChange}
              value={grind?.temperature}
            />
            <h5>Water/coffee ratio</h5>
            <Form.Group widths="equal">
              <p className="modal-content-body-water-coffee-ratio">
                1 cup
              </p>
              <Form.Input
                fluid
                name="coffee"
                type="number"
                onChange={handleInputChange}
                value={grind.coffee}
              />
              <p className="modal-content-body-water-coffee-ratio">Grams</p>
            </Form.Group>
            <Form.TextArea
              name="description"
              label="Description (Seperate steps with a ' / ')"
              placeholder="Description"
              rows={10}
              onChange={handleInputChange}
              value={grind?.description}
            />
          </Form>
        </div>
        <div className="modal-content-footer">
          <div className="buttons">
            <button className="button md primary" onClick={submitForm}>
              Ok
            </button>
            <button className="button md" onClick={grindStore.closeForm}>
              Cancel
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default observer(DefaultModal);
