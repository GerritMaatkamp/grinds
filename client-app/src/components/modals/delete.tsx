import React, { useContext, FormEvent, useState } from 'react';
import { IGrindV1 } from '../../App/models/grind';
import { ICoarsenessV1, coarsenessTypes } from '../../App/models/coarseness';
import { ICoffeeMakerV1, coffeeMakers } from '../../App/models/coffeeMaker';
import GrindStore from '../../App/stores/grindStore';
import { observer } from 'mobx-react-lite';
import { Form } from 'semantic-ui-react';
import { v4 as uuid } from 'uuid';
import '../../styles/modal.scss';

const DeleteModal = () => {
    const grindStore = useContext(GrindStore);
    let { deleteGrind, selectedGrind, closeDeleteDialog, disableScroll } = grindStore;
    
    disableScroll();

    const [grind, setGrind] = useState<IGrindV1 | undefined>(selectedGrind);

    return (
        <div className="modal">
            <div id="overlay" onClick={closeDeleteDialog}></div>
            <div className="modal-content">
                <div className="modal-content-header">
                    <h2>Are you sure you want to delete {grind?.title}</h2>
                </div>
                {/* <div className="modal-content-body">
                </div> */}
                <div className="modal-content-footer">
                    <div className="buttons">
                        <button className="button md primary" onClick={e => deleteGrind(selectedGrind)}>
                            Ok
                        </button>
                        <button className="button md" onClick={closeDeleteDialog}>
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default observer(DeleteModal);
