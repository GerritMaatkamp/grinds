using System;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Persistence;

namespace Application.Grinds
{
    public class Details
    {
        public class Query : IRequest<Grind>
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Query, Grind>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<Grind> Handle(Query request, CancellationToken cancellationToken)
            {
                var grind = await _context.Grinds.FindAsync(request.Id);

                return grind;
            }
        }
    }
}