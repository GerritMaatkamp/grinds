using System;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Persistence;

namespace Application.Grinds
{
    public class Create
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
            public string Title { get; set; }
            public string CoffeeMaker { get; set; }
            public int Coarseness { get; set; }
            public int Temperature { get; set; }
            public int Water { get; set; }
            public int Coffee { get; set; }
            public string Description { get; set; }
        }
        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var grind = new Grind
                {
                    Id = request.Id,
                    Title = request.Title,
                    CoffeeMaker = request.CoffeeMaker,
                    Coarseness = request.Coarseness,
                    Temperature = request.Temperature,
                    Water = request.Water,
                    Coffee = request.Coffee,
                    Description = request.Description,
                };

                _context.Grinds.Add(grind);
                var success = await _context.SaveChangesAsync() > 0;

                if (success) return Unit.Value;

                throw new Exception("Problem saving Grind changes");

            }
        }
    }

}