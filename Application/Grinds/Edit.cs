using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Persistence;

namespace Application.Grinds
{
    public class Edit
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
            public string Title { get; set; }
            public string CoffeeMaker { get; set; }
            public int Coarseness { get; set; }
            public int Temperature { get; set; }
            public int Water { get; set; }
            public int Coffee { get; set; }
            public string Description { get; set; }
        }
        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var grind = await _context.Grinds.FindAsync(request.Id);

                if (grind == null)
                    throw new Exception("Could not find exceptions");

                grind.Title = request.Title ?? grind.Title;
                grind.CoffeeMaker = request.CoffeeMaker ?? grind.CoffeeMaker;
                grind.Coarseness = (request.Coarseness > 0) ? request.Coarseness : grind.Coarseness;
                grind.Temperature = (request.Temperature > 0) ? request.Temperature : grind.Temperature;
                grind.Water = (request.Water > 0) ? request.Water : grind.Water;
                grind.Coffee = (request.Coffee > 0) ? request.Coffee : grind.Coffee;
                grind.Description = request.Description ?? grind.Description;

                var success = await _context.SaveChangesAsync() > 0;

                if (success) return Unit.Value;

                throw new Exception("Problem saving Grind changes");

            }
        }
    }
}