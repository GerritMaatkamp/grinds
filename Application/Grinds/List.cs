using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Grinds
{
    public class List
    {
        public class Query : IRequest<List<Grind>> { }

        public class Handler : IRequestHandler<Query, List<Grind>>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<List<Grind>> Handle(Query request, CancellationToken cancellationToken)
            {
                var grinds = await _context.Grinds.ToListAsync();

                return grinds;
            }
        }
    }
}