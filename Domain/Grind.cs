using System;
using System.Collections;
using System.Collections.Generic;

namespace Domain
{
    public class Grind
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string CoffeeMaker { get; set; }
        public int Coarseness { get; set; }
        public int Temperature { get; set; }
        public int Water { get; set; }
        public decimal Coffee { get; set; }
        public string Description { get; set; }
    }
}