Add an appsettings.json with 

{
  "ConnectionStrings": {
    "DefaultConnection": "Data source=grinds.db"
  },
  "Logging": {
    "LogLevel": {
      "Default": "Warning"
    }
  },
  "AllowedHosts": "*"
}
